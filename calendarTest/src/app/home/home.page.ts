import { Component, OnInit, ViewChild } from '@angular/core';
// import * as moment from 'moment';
import { CalendarService } from './calendar.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  monthName: any;
  firstDay: any;
  daysInMonth: number;
  days: any[];
  qtySpace: any[];

  constructor(private calendarService: CalendarService) {}

  ngOnInit() {
    this.setMonth();
  }

  getToday(){
    /*TODO: Implementar*/
  }

  setMonth(){
    this.monthName = this.calendarService.getMonth();
    this.firstDay = this.calendarService.getDay();
    this.daysInMonth = this.firstDay.daysInMonth();
    const dayOfWeek = this.firstDay.day();
    this.days = this.setDays(this.daysInMonth);
    this.qtySpace = Array(dayOfWeek - 1);
    console.log('hola juancarlo fday', this.firstDay);
    console.log('hola juancarlo', dayOfWeek);
  }

  setDays(qtyDays) {

    const arrayMonth: any[] = [];
    //arranca en viernes, por lo tanto necesito 4 dias en blanco del arreglo
    for (let index = 0; index < qtyDays; index++) {

      if(index === 0  ) {
        arrayMonth.push(this.firstDay);
      } else{
        arrayMonth.push(arrayMonth[index - 1].clone().add( 1,'days' ));
      }
    }
    return arrayMonth;
  }

}
