import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  initialDay: any;
  today: any;
  month: 0;
  year: 2020;
  monthsWith31: number[]=[1,3,5,7,8,10,12];


  constructor() {
    this.calendarInit();
  }


  calendarInit() {
    moment.locale('es');
    this.initialDay = moment('2021-01-01');
    this.today = moment('2020-01-31');
  }


  getDay(){

    if(this.initialDay > this.today){
      this.today = this.today.clone().add(1,'days');
      return this.initialDay;
    }

    this.today = this.today.clone().add(1,'days');

    console.log(this.today);

    return this.today;
  }

  getMonth() {

    this.month = this.initialDay.month(`${0}`).format('MMMM');
    return this.month;

  }
}